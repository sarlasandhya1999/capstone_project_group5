 create  database capstoneproject;
use capstoneproject;
create table feature_table (
       feature_id integer not null auto_increment,
        feature_name varchar(255),
        primary key (feature_id)
    );
create table message_table (
       message_id integer not null auto_increment,
        message varchar(255),
        reply varchar(255),
        primary key (message_id)
    );
 create table owner_message (
       owner_id integer not null,
        message_id integer not null
    ) ;
create table owner_property (
       owner_id integer not null,
        property_id integer not null
    );
create table owner_table (
       owner_id integer not null auto_increment,
        owner_contact double precision not null,
        owner_email varchar(255),
        owner_name varchar(255),
        owner_password varchar(255),
        primary key (owner_id)
    );
create table property_features (
       property_id integer not null,
        features_id integer not null
    );
 create table property_reserve (
       reservation_id integer not null,
        property_id integer not null
    );
 create table property_table (
       property_id integer not null auto_increment,
        address varchar(255),
        city_name varchar(255),
        features varchar(255),
        image varchar(255),
        price double precision not null,
        property_name varchar(255),
        property_rating double precision not null,
        user_id integer,
        primary key (property_id)
    );
 create table reservation_table (
       reserve_id integer not null auto_increment,
        check_in_date varchar(255),
        check_out_date varchar(255),
        reserved_status varchar(255),
        primary key (reserve_id)
    );
create table user_message (
       user_id integer not null,
        message_id integer not null
    );
 create table user_table (
       user_id integer not null auto_increment,
        user_contact_no double precision not null,
        user_email varchar(255),
        user_name varchar(255),
        user_password varchar(255),
        primary key (user_id)
    );