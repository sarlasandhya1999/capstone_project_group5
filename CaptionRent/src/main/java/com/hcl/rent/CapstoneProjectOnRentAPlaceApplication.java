package com.hcl.rent;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CapstoneProjectOnRentAPlaceApplication {

	public static void main(String[] args) {
		SpringApplication.run(CapstoneProjectOnRentAPlaceApplication.class, args);
	}

}
