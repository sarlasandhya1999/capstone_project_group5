package com.hcl.rent.beans;


import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;
import lombok.NoArgsConstructor;

@Entity(name="message_table")
@Data
@NoArgsConstructor
public class Message {
	@Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
	private int messageId;
	private String message;
	private String reply;

	
	
