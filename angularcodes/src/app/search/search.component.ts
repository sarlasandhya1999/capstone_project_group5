import { Component, OnInit } from '@angular/core';
import { SearchServiceService } from '../search-service.service';

import { Property } from '../Owner';
@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {
  flag:boolean=false;
  search:Property=new Property();
  Featched!:boolean;
  msg='';
  properties: Array<Property> = [];
  selectedProperty:Property |any;
  action: string="";

    constructor(private service:SearchServiceService) { }
  
    ngOnInit(): void {
      this.Featched=false;
    }
  ser()
  {
    this.service.ser(this.search.propertyName).subscribe(data=>{
    this.Featched=true,  
    this.search=data})
  }
  click : boolean = false;

  
 
  
  onButtonClick(){
    this.click = !this.click;
  }
  userreservationform()
  {
    this.click=!this.click;
  }

  serv()
{
this.flag=true;
}
handleSuccessfulResponse(response:any) {
 this.properties = response;
}
 
}
