export class Owner {
    ownerId: number=0;

    ownerName: string="";

    ownerEmail:string="";

    ownerPassword: string="";

    ownerContact:number=0;
}


export class Property{

    propertyId: number=0;

    propertyName:string="";
    price:number=0;
    propertyRating:number=0;
    features:string="";
    cityName:string="";

    address:string="";

    image:string="";

}



export class Feature{

    featureId: number=0;

    featureName: string="";

}



export class Reservation{
    name:string="";
    propertyName:string="";

   
    
   reserveId:number=0;

   reservedStatus:string="";

   checkInDate: Date|any;

   checkOutDate:Date|any;
   tenantEmail:string="";

}
    export class Message{
        name:string="";
     messageId: number=0;
	 message: string="";
	 reply: string="";
    }
    
    export class OwnerLogin{

        ownerId: number=0;
    
        ownerName: string="";
    
        ownerEmail:string="";

        ownerPassword: string="";
    
        ownerContact:number=0;
        constructor(){}
    }


export class Cart
{
    propertyId: number=0;

    propertyName:string="";
    price:number=0;
    propertyRating:number=0;
    features:string="";
    cityName:string="";

    address:string="";

    image:string="";
    tenantEmail:string="";
    constructor()
    {}   
}



    // ownerReply:{messageId: number,message: string,reply: string},

   

    // addProperty:{propertyId:number,propertyName: string,propertyRating: number,

    //     featureList: {featureId: number,featureName: string},

    //     reservedPropertyList: {reserveId:number,reservedStatus:string,checkInDate:Date,checkOutDate:Date}}

     