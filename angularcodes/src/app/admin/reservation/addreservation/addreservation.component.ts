import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { Reservation } from 'src/app/Owner';
import { HttpClientService } from 'src/app/service/http-client.service';

@Component({
  selector: 'app-addreservation',
  templateUrl: './addreservation.component.html',
  styleUrls: ['./addreservation.component.css']
})
export class AddreservationComponent implements OnInit {
msg='';
  @Input()
  reservation:Reservation ={
    reserveId:0,
    reservedStatus:"",
    tenantEmail:"",
    checkInDate:"",
    checkOutDate:"",
    propertyName:"",
    name:"",
   
  };
  @Output()
  reservationAddedEvent = new EventEmitter();
  
  constructor(private httpClientService: HttpClientService,
    private router: Router) { }

  ngOnInit(): void {
  }
  addReservation() {
    this.httpClientService.addReservation(this.reservation).subscribe(
      (reservation) => {
        this.reservationAddedEvent.emit();
        this.msg="Booked Successfully";
        this.router.navigate(['admin','reservations']);
      }
    );
  }
}