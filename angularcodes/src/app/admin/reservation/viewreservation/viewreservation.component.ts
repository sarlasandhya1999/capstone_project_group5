import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { Reservation } from 'src/app/Owner';
import { HttpClientService } from 'src/app/service/http-client.service';

@Component({
  selector: 'app-viewreservation',
  templateUrl: './viewreservation.component.html',
  styleUrls: ['./viewreservation.component.css']
})
export class ViewreservationComponent implements OnInit {
  public show:boolean = false;
  public hide:boolean = true;
  public buttonName:any = 'Show';
  public nameT:any = 'Hide';
  @Input()
  reservation:Reservation={
    reserveId:0,
    tenantEmail:"",
    reservedStatus:"",
    checkOutDate:"",
    checkInDate:"",
    propertyName:"",
    name:"",
   
  };
  @Output()
  reservationDeletedEvent = new EventEmitter();
  constructor(private httpClientService: HttpClientService,
    private router: Router) { }

  ngOnInit(): void {
  }
  deleteReservation() {
    this.httpClientService.deleteReservation(this.reservation.reserveId).subscribe(
      (reservation) => {
        this.reservationDeletedEvent.emit();
        this.router.navigate(['admin', 'reservations']);
      }
    );
  }
  editReservation() {
    this.router.navigate(['admin', 'reservations'], { queryParams: { action: 'edit', id: this.reservation.reserveId } });
  }

confirmMail()
{
  console.log("hi");
}
Hide()
{
  this.hide=!this.hide;
  if(this.hide)
  {
    
    this.nameT= "Hide";

  }
  else{
    this.nameT = "Show";
  }
}
}
