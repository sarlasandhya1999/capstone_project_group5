import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Reservation } from 'src/app/Owner';
import { HttpClientService } from 'src/app/service/http-client.service';

@Component({
  selector: 'app-reservation',
  templateUrl: './reservation.component.html',
  styleUrls: ['./reservation.component.css']
})
export class ReservationComponent implements OnInit {
  public show:boolean = false;
  public hide:boolean = true;
  public buttonName:any = 'Show';
  public nameT:any = 'Hide';
 
  reservations: Array<Reservation> = [];
  selectedReservation:Reservation |any;
  action: string="";
sentmsg:string="";
  constructor(private httpClientService: HttpClientService,private router: Router,
    private activatedRoute: ActivatedRoute) { }

    ngOnInit() {
      this.refreshData();
     }
   
     refreshData() {
       this.httpClientService.getReservations().subscribe(
         response => this.handleSuccessfulResponse(response),
       );
   
       this.activatedRoute.queryParams.subscribe(
         (params) => {
           this.action = params['action']
           const selectedReservationId = params['id'];
           if (selectedReservationId) {
             this.selectedReservation= this.reservations.find(reservation => reservation.reserveId === +selectedReservationId);
           }
         }
       );
     }

  handleSuccessfulResponse(response:any) {
    this.reservations = response;
  }

  addReservation() {
    this.selectedReservation = new Reservation();
    this.router.navigate(['admin', 'reservations'], { queryParams: { action: 'add' } });
  }
  viewReservation(id: number) {
    this.router.navigate(['admin','reservations'], {queryParams : {id, action: 'view'}});
  }
  cofirmMail(tenantEmail:string)
  {
    this.httpClientService.sendMail(tenantEmail).subscribe(error=>console.log(error));
  }
  Hide()
{
  this.hide=!this.hide;
  if(this.hide)
  {
    
    this.nameT= "Hide";

  }
  else{
    this.nameT = "Show";
  }
}
}
