
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { Message } from 'src/app/Owner';
import { HttpClientService } from 'src/app/service/http-client.service';

@Component({
  selector: 'app-viewmessage',
  templateUrl: './viewmessage.component.html',
  styleUrls: ['./viewmessage.component.css']
})
export class ViewmessageComponent implements OnInit {

 
  @Input()
  message:Message={
    messageId:0,
    message:"",
    reply:"",
    name:""
  };
  @Output()
  messageDeletedEvent = new EventEmitter();
  constructor(private httpClientService: HttpClientService,
    private router: Router) { }

  ngOnInit(): void {
  }
  deleteMessage() {
    this.httpClientService.deleteMessage(this.message.messageId).subscribe(
      (message) => {
        this.messageDeletedEvent.emit();
        this.router.navigate(['admin', 'messages']);
      }
    );
  }
  editMessage() {
    this.router.navigate(['admin', 'messages'], { queryParams: { action: 'edit', id: this.message.messageId } });
  }

}
