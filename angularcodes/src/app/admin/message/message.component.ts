import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Message } from 'src/app/Owner';
import { HttpClientService } from 'src/app/service/http-client.service';

@Component({
  selector: 'app-message',
  templateUrl: './message.component.html',
  styleUrls: ['./message.component.css']
})
export class MessageComponent implements OnInit {

  messages: Array<Message> = [];
  selectedMessage:Message |any;
  action: string="";

  constructor(private httpClientService: HttpClientService,private router: Router,
    private activatedRoute: ActivatedRoute) { }

    ngOnInit() {
      this.refreshData();
     }
   
     refreshData() {
       this.httpClientService.getMessage().subscribe(
         response => this.handleSuccessfulResponse(response),
       );
   
       this.activatedRoute.queryParams.subscribe(
         (params) => {
           this.action = params['action']
           const selectedMessageId = params['id'];
           if (selectedMessageId) {
             this.selectedMessage = this.messages.find(message => message.messageId === +selectedMessageId);
           }
         }
       );
     }

  handleSuccessfulResponse(response:any) {
    this.messages = response;
  }

  addMessage() {
    this.selectedMessage = new Message();
    this.router.navigate(['admin', 'messages'], { queryParams: { action: 'add' } });
  }
  viewMessage(id: number) {
    this.router.navigate(['admin','messages'], {queryParams : {id, action: 'view'}});
  }


}
