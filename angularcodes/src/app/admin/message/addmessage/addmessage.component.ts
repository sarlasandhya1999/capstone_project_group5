import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { message } from 'src/app/message';
import { Message } from 'src/app/Owner';
import { HttpClientService } from 'src/app/service/http-client.service';

@Component({
  selector: 'app-addmessage',
  templateUrl: './addmessage.component.html',
  styleUrls: ['./addmessage.component.css']
})
export class AddmessageComponent implements OnInit {
  @Input()
  message:Message ={
    messageId:0,
    message:"",
    reply:"",
    name:""
  
  };
  @Output()
  messageAddedEvent = new EventEmitter();
  
  constructor(private httpClientService: HttpClientService,
    private router: Router) { }

  ngOnInit(): void {
  }
  addMessage() {
    this.httpClientService.addMessage(this.message).subscribe(
      (message) => {
        this.messageAddedEvent.emit();
        this.router.navigate(['admin', 'messages']);
      }
    );
  }
}
