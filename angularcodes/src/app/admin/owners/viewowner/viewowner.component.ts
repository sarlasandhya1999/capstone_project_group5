import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { Owner } from 'src/app/Owner';
import { HttpClientService } from 'src/app/service/http-client.service';

@Component({
  selector: 'app-viewowner',
  templateUrl: './viewowner.component.html',
  styleUrls: ['./viewowner.component.css']
})
export class ViewownerComponent implements OnInit {
  @Input()
  owner: Owner={
    ownerId:0,
    ownerName:"",
    ownerEmail:"",
    ownerPassword:"",
    ownerContact:0
  };
  @Output()
  ownerDeletedEvent = new EventEmitter();
  constructor(private httpClientService: HttpClientService,
    private router: Router) { }

  ngOnInit(): void {
  }
  deleteOwner() {
    this.httpClientService.deleteOwner(this.owner.ownerId).subscribe(
      (owner) => {
        this.ownerDeletedEvent.emit();
        this.router.navigate(['admin', 'owners']);
      }
    );
  }
  editOwner() {
    this.router.navigate(['admin', 'owners'], { queryParams: { action: 'edit', id: this.owner.ownerId } });
  }

}
