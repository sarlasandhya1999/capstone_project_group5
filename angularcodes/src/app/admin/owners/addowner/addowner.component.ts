import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { Owner } from 'src/app/Owner';
import { HttpClientService } from 'src/app/service/http-client.service';

@Component({
  selector: 'app-addowner',
  templateUrl: './addowner.component.html',
  styleUrls: ['./addowner.component.css']
})
export class AddownerComponent implements OnInit {
  @Input()
  owner: Owner ={
    ownerId:0,
    ownerName:"",
    ownerEmail:"",
    ownerPassword:"",
    ownerContact:0
  };
  @Output()
  ownerAddedEvent = new EventEmitter();
  
  constructor(private httpClientService: HttpClientService,
    private router: Router) { }

  ngOnInit(): void {
  }
  addOwner() {
    this.httpClientService.addOwner(this.owner).subscribe(
      (owner) => {
        this.ownerAddedEvent.emit();
        this.router.navigate(['admin','owners'])
        
      }
    );
  }
}
