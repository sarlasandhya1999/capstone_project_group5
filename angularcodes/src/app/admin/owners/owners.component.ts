import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import {Owner, Property } from 'src/app/Owner';
import { HttpClientService } from 'src/app/service/http-client.service';

@Component({
  selector: 'app-owners',
  templateUrl: './owners.component.html',
  styleUrls: ['./owners.component.css']
})
export class OwnersComponent implements OnInit {

  owners: Array<Owner> = [];
  selectedOwner:Owner |any;
  action: string="";

  constructor(private httpClientService: HttpClientService,private router: Router,
    private activatedRoute: ActivatedRoute) { }

    ngOnInit() {
      this.refreshData();
     }
   
     refreshData() {
       this.httpClientService.getOwners().subscribe(
         response => this.handleSuccessfulResponse(response),
       );
   
       this.activatedRoute.queryParams.subscribe(
         (params) => {
           this.action = params['action']
           const selectedOwnerId = params['id'];
           if (selectedOwnerId) {
             this.selectedOwner = this.owners.find(owner => owner.ownerId === +selectedOwnerId);
           }
         }
       );
     }

  handleSuccessfulResponse(response:any) {
    this.owners = response;
  }

  addOwner() {
    this.selectedOwner = new Owner();
    this.router.navigate(['admin', 'owners'], { queryParams: { action: 'add' } });
  }
  viewOwner(id: number) {
    this.router.navigate(['admin','owners'], {queryParams : {id, action: 'view'}});
  }
}
