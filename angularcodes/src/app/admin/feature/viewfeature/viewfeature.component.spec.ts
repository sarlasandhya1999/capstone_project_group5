import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewfeatureComponent } from './viewfeature.component';

describe('ViewfeatureComponent', () => {
  let component: ViewfeatureComponent;
  let fixture: ComponentFixture<ViewfeatureComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ViewfeatureComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewfeatureComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
