import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { Feature } from 'src/app/Owner';
import { HttpClientService } from 'src/app/service/http-client.service';

@Component({
  selector: 'app-viewfeature',
  templateUrl: './viewfeature.component.html',
  styleUrls: ['./viewfeature.component.css']
})
export class ViewfeatureComponent implements OnInit {

  @Input()
  feature:Feature={
    featureId:0,
    featureName:""
  };
  @Output()
  featureDeletedEvent = new EventEmitter();
  constructor(private httpClientService: HttpClientService,
    private router: Router) { }

  ngOnInit(): void {
  }
  deleteFeature() {
    this.httpClientService.deleteFeature(this.feature.featureId).subscribe(
      (feature) => {
        this.featureDeletedEvent.emit();
        this.router.navigate(['admin', 'features']);
      }
    );
  }
  editFeature() {
    this.router.navigate(['admin', 'features'], { queryParams: { action: 'edit', id: this.feature.featureId } });
  }
}
