import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpClientService } from 'src/app/service/http-client.service';
import { Feature } from 'src/app/Owner';
@Component({
  selector: 'app-feature',
  templateUrl: './feature.component.html',
  styleUrls: ['./feature.component.css']
})
export class FeatureComponent implements OnInit {
  features: Array<Feature> = [];
  selectedFeature:Feature |any;
  action: string="";

  constructor(private httpClientService: HttpClientService,private router: Router,
    private activatedRoute: ActivatedRoute) { }

    ngOnInit() {
      this.refreshData();
     }
   
     refreshData() {
       this.httpClientService.getFeatures().subscribe(
         response => this.handleSuccessfulResponse(response),
       );
   
       this.activatedRoute.queryParams.subscribe(
         (params) => {
           this.action = params['action']
           const selectedFeatureId = params['id'];
           if (selectedFeatureId) {
             this.selectedFeature = this.features.find(feature => feature.featureId === +selectedFeatureId);
           }
         }
       );
     }

  handleSuccessfulResponse(response:any) {
    this.features = response;
  }

  addFeature() {
    this.selectedFeature = new Feature();
    this.router.navigate(['admin', 'features'], { queryParams: { action: 'add' } });
  }
  viewFeature(id: number) {
    this.router.navigate(['admin','features'], {queryParams : {id, action: 'view'}});
  }

}
