import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { Feature } from 'src/app/Owner';
import { HttpClientService } from 'src/app/service/http-client.service';

@Component({
  selector: 'app-addfeature',
  templateUrl: './addfeature.component.html',
  styleUrls: ['./addfeature.component.css']
})
export class AddfeatureComponent implements OnInit {

  @Input()
  feature:Feature ={
    featureId:0,
    featureName:""
  
  };
  @Output()
  featureAddedEvent = new EventEmitter();
  
  constructor(private httpClientService: HttpClientService,
    private router: Router) { }

  ngOnInit(): void {
  }
  addFeature() {
    this.httpClientService.addFeature(this.feature).subscribe(
      (feature) => {
        this.featureAddedEvent.emit();
        this.router.navigate(['admin', 'features']);
      }
    );
  }
}

