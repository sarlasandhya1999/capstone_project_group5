import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Property } from 'src/app/Owner';
import { HttpClientService } from 'src/app/service/http-client.service';

@Component({
  selector: 'app-property',
  templateUrl: './property.component.html',
  styleUrls: ['./property.component.css']
})
export class PropertyComponent implements OnInit {
  public show:boolean = false;
  public hide:boolean = true;
  public buttonName:any = 'Show';
  public nameT:any = 'Hide';
 
  properties: Array<Property> = [];
  selectedProperty:Property |any;
  action: string="";

  constructor(private httpClientService: HttpClientService,private router: Router,
    private activatedRoute: ActivatedRoute) { }

    ngOnInit() {
      this.refreshData();
     }
   
     refreshData() {
       this.httpClientService.getProperties().subscribe(
         response => this.handleSuccessfulResponse(response),
       );
   
       this.activatedRoute.queryParams.subscribe(
         (params) => {
           this.action = params['action']
           const selectedPropertyId = params['id'];
           if (selectedPropertyId) {
             this.selectedProperty = this.properties.find(property => property.propertyId === +selectedPropertyId);
           }
         }
       );
     }

  handleSuccessfulResponse(response:any) {
    this.properties = response;
  }

  addProperty() {
    this.selectedProperty = new Property();
    this.router.navigate(['admin', 'properties'], { queryParams: { action: 'add' } });
  }
  viewProperty(id: number) {
    this.router.navigate(['admin','properties'], {queryParams : {id, action: 'view'}});
  }
  Hide()
{
  this.hide=!this.hide;
  if(this.hide)
  {
    
    this.nameT= "Hide";

  }
  else{
    this.nameT = "Show";
  }
}
}
