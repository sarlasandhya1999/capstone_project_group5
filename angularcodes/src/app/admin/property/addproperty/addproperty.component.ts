import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { Property } from 'src/app/Owner';
import { HttpClientService } from 'src/app/service/http-client.service';

@Component({
  selector: 'app-addproperty',
  templateUrl: './addproperty.component.html',
  styleUrls: ['./addproperty.component.css']
})
export class AddpropertyComponent implements OnInit {

  @Input()
  property:Property ={
    propertyId: 0,
    propertyName: "",
    propertyRating: 0,
    price:0,
    features:"",
    cityName: '',
    address: '',
    image: ''
  };
  @Output()
  propertyAddedEvent = new EventEmitter();
  
  constructor(private httpClientService: HttpClientService,
    private router: Router) { }

  ngOnInit(): void {
  }
  addProperty() {
    this.httpClientService.addProperty(this.property).subscribe(
      (property) => {
        this.propertyAddedEvent.emit();
        this.router.navigate(['admin', 'properties']);
      }
    );
  }
}


