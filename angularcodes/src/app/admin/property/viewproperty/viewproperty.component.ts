import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { Property } from 'src/app/Owner';
import { HttpClientService } from 'src/app/service/http-client.service';

@Component({
  selector: 'app-viewproperty',
  templateUrl: './viewproperty.component.html',
  styleUrls: ['./viewproperty.component.css']
})
export class ViewpropertyComponent implements OnInit {
  @Input()
  property:Property={
    propertyId: 0,
    propertyName: "",
    propertyRating: 0,
    features:"",
    price:0,
    cityName: '',
    address: '',
    image: ''
  };
  @Output()
  propertyDeletedEvent = new EventEmitter();
  constructor(private httpClientService: HttpClientService,
    private router: Router) { }

  ngOnInit(): void {
  }
  deleteProperty() {
    this.httpClientService.deleteProperty(this.property.propertyId).subscribe(
      (property) => {
        this.propertyDeletedEvent.emit();
        this.router.navigate(['admin', 'properties']);
      }
    );
  }
  editProperty() {
    this.router.navigate(['admin', 'properties'], { queryParams: { action: 'edit', id: this.property.propertyId } });
  }

}
