import { Injectable } from '@angular/core';

import { HttpClient } from '@angular/common/http';

import { Cart, Feature, Owner, Property, Reservation } from '../Owner';
import { Message } from '../Owner';
import { Observable } from 'rxjs';
import { Tenant } from '../tenant';
 


@Injectable({

  providedIn: 'root'

})

export class HttpClientService {

 

  constructor(private httpClient:HttpClient)

  {

     }

 

 getOwners()

  {

    return this.httpClient.get<Owner[]>('http://localhost:8080/owners/getOwners');

  }

  addOwner(newOwner: Owner) {

    return this.httpClient.post<Owner>('http://localhost:8080/owners/addOwner', newOwner);  

  }

  deleteOwner(ownerId:number) {

    return this.httpClient.delete<Owner>('http://localhost:8080/owners/deleteOwner/' +ownerId);

  }

  updateOwner(updatedOwner: Owner) {

    return this.httpClient.put<Owner>('http://localhost:8080/owners/updateOwner', updatedOwner);

  }

  getProperties()  {    return this.httpClient.get<Property[]>('http://localhost:8080/owners/getProperty');  }  
  addProperty(newProperty: Property) {    return this.httpClient.post<Property>('http://localhost:8080/owners/addProperty', newProperty);     }  deleteProperty(propertyId:number) {    return this.httpClient.delete<Property>('http://localhost:8080/owners/deleteProperty/' +propertyId);  }  updateProperty(updatedProperty: Property) {    return this.httpClient.put<Property>('http://localhost:8080/owners/updateProperty', updatedProperty);  }
  
  
   getFeatures()  {    return this.httpClient.get<Feature[]>('http://localhost:8080/owners/getFeatures');  }  
  addFeature(newFeature: Feature) {    return this.httpClient.post<Feature>('http://localhost:8080/owners/addFeature', newFeature);     } 
   deleteFeature(featureId:number) {    return this.httpClient.delete<Feature>('http://localhost:8080/owners/deleteFeature/' +featureId);  }  
   updateFeature(updatedFeature: Feature) {    return this.httpClient.put<Feature>('http://localhost:8080/owners/updateFeature', updatedFeature);  }
  
  
   
 getMessage()

 {

   return this.httpClient.get<Message[]>('http://localhost:8080/owners/getMessage');

 }

 addMessage(newMessage: Message) {

   return this.httpClient.post<Owner>('http://localhost:8080/owners/addMessage',newMessage);  

 }

 deleteMessage(messageId:number) {

   return this.httpClient.delete<Owner>('http://localhost:8080/owners/deleteMessage/' +messageId);

 }

 updateMessage(updatedMessage: Message) {

   return this.httpClient.put<Owner>('http://localhost:8080/owners/updateMessage', updatedMessage);

 }

 public loginUserFromRemote(owner:Owner):Observable<any>
 {
return this.httpClient.post<any>("http://localhost:8080/owners/login",owner);
 }

 public loginTenantFromRemote(tenants:Tenant):Observable<any>
 {
  return this.httpClient.post<any>("http://localhost:8080/tenants/login",tenants);
   }
 getReservations()

  {

    return this.httpClient.get<Reservation[]>('http://localhost:8080/owners/getReservation');

  }

  addReservation(newReservation: Reservation) {

    return this.httpClient.post<Reservation>('http://localhost:8080/owners/addReservation', newReservation);  

  }

  deleteReservation(reserveId:number) {

    return this.httpClient.delete<Reservation>('http://localhost:8080/owners/deleteReservation/' +reserveId);

  }

  updateReservation(updatedReservation: Reservation) {

    return this.httpClient.put<Reservation>('http://localhost:8080/owners/updateReservation', updatedReservation);

  }

  addTenant(newTenant: Tenant) {

    return this.httpClient.post<Tenant>('http://localhost:8080/tenants/registerTenant', newTenant);  

  }
  sendMail(tenantEmail:string) {

    return this.httpClient.get<Reservation>('http://localhost:8080/email/simple-email/' +tenantEmail);

  }
  addCart(newCart: Cart) 
  {    return this.httpClient.post<Property>('http://localhost:8080/tenants/addcart', newCart);     }
}