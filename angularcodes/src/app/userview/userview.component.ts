import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Message } from '../Owner';
import { HttpClientService } from '../service/http-client.service';

@Component({
  selector: 'app-userview',
  templateUrl: './userview.component.html',
  styleUrls: ['./userview.component.css']
})
export class UserviewComponent implements OnInit {

  messages: Array<Message> = [];
  selectedMessage:Message |any;
  action: string="";

  constructor(private httpClientService: HttpClientService,private router: Router,
    private activatedRoute: ActivatedRoute) { }

    ngOnInit() {
      this.refreshData();
     }
   
     refreshData() {
       this.httpClientService.getMessage().subscribe(
         response => this.handleSuccessfulResponse(response),
       );
   
       this.activatedRoute.queryParams.subscribe(
         (params) => {
           this.action = params['action']
           const selectedMessageId = params['id'];
           if (selectedMessageId) {
             this.selectedMessage = this.messages.find(message => message.messageId === +selectedMessageId);
           }
         }
       );
     }

  handleSuccessfulResponse(response:any) {
    this.messages = response;
  }

  }
