import {message} from "./message"
export class Tenant {
    public tenantId!:number;
	public tenantName!:string;
    public tenantPassword!:string;
    public tenantEmail!:string;
	public tenantContactNo!:number;
}
