import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { Owner, OwnerLogin } from '../Owner';
import { HttpClientService } from '../service/http-client.service';
@Component({
  selector: 'app-ulogin',
  templateUrl: './ulogin.component.html',
  styleUrls: ['./ulogin.component.css']
})
export class UloginComponent implements OnInit {
owners=new OwnerLogin();
msg='';
  constructor(private httpClientService: HttpClientService,private router:Router) { }

  ngOnInit(): void {
  }
loginUser()
{
this.httpClientService.loginUserFromRemote(this.owners).subscribe(
  data =>{ console.log("response recived");
this.router.navigate(['/ownerlogin'])   },
  error => {console.log("exception occured");
this.msg="Bad credentials,please add proper credentials";
}
)
}
}
