import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Property } from '../Owner';
import { HttpClientService } from '../service/http-client.service';

@Component({
  selector: 'app-userproperty',
  templateUrl: './userproperty.component.html',
  styleUrls: ['./userproperty.component.css']
})
export class UserpropertyComponent implements OnInit {
  properties: Array<Property> = [];
  selectedProperty:Property |any;
  action: string="";flag:boolean=false;

  constructor(private httpClientService: HttpClientService,private router: Router,
    private activatedRoute: ActivatedRoute) { }

    ngOnInit() {
      this.refreshData();
     }
   
     refreshData() {
       this.httpClientService.getProperties().subscribe(
         response => this.handleSuccessfulResponse(response),
       );
   
       this.activatedRoute.queryParams.subscribe(
         (params) => {
           this.action = params['action']
           const selectedPropertyId = params['id'];
           if (selectedPropertyId) {
             this.selectedProperty = this.properties.find(property => property.propertyId === +selectedPropertyId);
           }
         }
       );
     }
 serv()
 {
  this.flag=true;
 }
  handleSuccessfulResponse(response:any) {
    this.properties = response;
  }
 
}
