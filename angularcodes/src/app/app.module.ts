import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FeatureComponent } from './admin/feature/feature.component';
import { AddfeatureComponent } from './admin/feature/addfeature/addfeature.component';
import { ViewfeatureComponent } from './admin/feature/viewfeature/viewfeature.component';
import { MessageComponent } from './admin/message/message.component';
import { AddmessageComponent } from './admin/message/addmessage/addmessage.component';
import { ViewmessageComponent } from './admin/message/viewmessage/viewmessage.component';
import { OwnersComponent } from './admin/owners/owners.component';
import { AddownerComponent } from './admin/owners/addowner/addowner.component';
import { ViewownerComponent } from './admin/owners/viewowner/viewowner.component';
import { PropertyComponent } from './admin/property/property.component';
import { AddpropertyComponent } from './admin/property/addproperty/addproperty.component';
import { ViewpropertyComponent } from './admin/property/viewproperty/viewproperty.component';
import { ReservationComponent } from './admin/reservation/reservation.component';
import { AddreservationComponent } from './admin/reservation/addreservation/addreservation.component';
import { ViewreservationComponent } from './admin/reservation/viewreservation/viewreservation.component';
import { CardComponent } from './card/card.component';
import { ContactComponent } from './contact/contact.component';
import { CoroselComponent } from './corosel/corosel.component';
import { FeturesComponent } from './fetures/fetures.component';
import { FooterComponent } from './footer/footer.component';
import { HeaderComponent } from './header/header.component';
import { MenuComponent } from './menu/menu.component';
import { PropertyComponent1 } from './property1/property1.component';
import { SearchComponent } from './search/search.component';
import { TenantLoginComponent } from './tenant-login/tenant-login.component';
import { UloginComponent } from './ulogin/ulogin.component';
import { UserregisterComponent } from './userregister/userregister.component';
import { UserreservationComponent } from './userreservation/userreservation.component';
import { UserviewComponent } from './userview/userview.component';
import { HttpClientModule } from '@angular/common/http';
import { UserpropertyComponent } from './userproperty/userproperty.component';
@NgModule({
  declarations: [
    AppComponent,
    FeatureComponent,
    AddfeatureComponent,
    ViewfeatureComponent,
    MessageComponent,
    AddmessageComponent,
    ViewmessageComponent,
    OwnersComponent,
    AddownerComponent,
    ViewownerComponent,
    PropertyComponent,
    AddpropertyComponent,
    ViewpropertyComponent,
    ReservationComponent,
    AddreservationComponent,
    ViewreservationComponent,
    CardComponent,
    ContactComponent,
    CoroselComponent,
    FeturesComponent,
    FooterComponent,
    HeaderComponent,
    MenuComponent,
    PropertyComponent1,
    SearchComponent,
    TenantLoginComponent,
    UloginComponent,
    UserregisterComponent,
    UserreservationComponent,
    UserviewComponent,
    UserpropertyComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,FormsModule,HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
