import {features} from "./features"
export class Property {

    public propertyId!:number;
	public propertyName!:string;
    public propertyRating!:number;
    public searchId!:number;
    public featureList!:features[];

    
    constructor(propertyId:number, propertyName:string,propertyRating:number,searchId:number, featureList:features[])
    {

    }
}
