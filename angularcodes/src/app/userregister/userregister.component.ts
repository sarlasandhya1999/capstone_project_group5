import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClientService } from '../service/http-client.service';
import { Tenant } from '../tenant';

@Component({
  selector: 'app-userregister',
  templateUrl: './userregister.component.html',
  styleUrls: ['./userregister.component.css']
})
export class UserregisterComponent implements OnInit {

  @Input()
  tenant: Tenant ={
    tenantId:0,
    tenantName:"",
    tenantEmail:"",
    tenantPassword:"",
    tenantContactNo:0
  };
  @Output()
  ownerAddedEvent = new EventEmitter();
  
  constructor(private httpClientService: HttpClientService,
    private router: Router) { }

  ngOnInit(): void {
  }
  addTenant() {
    this.httpClientService.addTenant(this.tenant).subscribe(
      (tenant) => {
        this.ownerAddedEvent.emit();
        this.router.navigate(['search/tlogin'])
        
      }
    );
  }
}
