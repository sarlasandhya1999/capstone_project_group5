import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { Message } from '../Owner';
import { HttpClientService } from '../service/http-client.service';
import { Property } from '../Owner';
import { SearchServiceService } from '../search-service.service';


@Component({
  selector: 'app-corosel',
  templateUrl: './corosel.component.html',
  styleUrls: ['./corosel.component.css']
})
export class CoroselComponent implements OnInit {
  search:Property=new Property();
  Featched!:boolean;
  public hide:boolean = true;
  public buttonName:any = 'Show';
  public nameT:any = 'Hide';
  @Input()
  message:Message ={
    messageId:0,
    message:"",
    reply:"",
    name:""
  
  };
  
  @Output()
  messageAddedEvent = new EventEmitter();
  
  constructor(private httpClientService: HttpClientService,
    private router: Router,private service:SearchServiceService) { }

  ngOnInit(): void {
  }
  addMessage() {
    this.httpClientService.addMessage(this.message).subscribe(
      (message) => {
        this.messageAddedEvent.emit();
        
      }
    );
  }
  Hide()
  {
    this.hide=!this.hide;
    if(this.hide)
    {
      
      this.nameT= "Hide";

    }
    else{
      this.nameT = "Show";
    }
  }
  ser()
  {
    this.service.ser(this.search.propertyName).subscribe(data=>{
    this.Featched=true,  
    this.search=data})
  }
  click : boolean = false;

  onButtonClick(){
    this.click = !this.click;
  }

}
