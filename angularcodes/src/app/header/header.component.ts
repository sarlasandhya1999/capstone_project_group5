import { Component, OnInit } from '@angular/core';
import { Property } from '../Owner';
import { SearchServiceService } from '../search-service.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  search:Property=new Property();
  Featched!:boolean;
  public hide:boolean = true;
  public buttonName:any = 'Show';
  public nameT:any = 'Hide';
  constructor(private service:SearchServiceService) { }

  ngOnInit(): void {
    this.Featched=false;
  }
  ser()
  {
    this.service.ser(this.search.propertyName).subscribe(data=>{
    this.Featched=true,  
    this.search=data})
  }
  click : boolean = false;

  onButtonClick(){
    this.click = !this.click;
  }

}
