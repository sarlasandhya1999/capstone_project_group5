import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { Cart, Reservation } from '../Owner';
import { HttpClientService } from '../service/http-client.service';
import { SearchServiceService } from '../search-service.service';
import { Property } from '../Owner';
@Component({
  selector: 'app-userreservation',
  templateUrl: './userreservation.component.html',
  styleUrls: ['./userreservation.component.css']
})
export class UserreservationComponent implements OnInit {
  msg='';
  search:Property=new Property();
  Featched!:boolean;
  properties: Array<Property> = [];
  selectedProperty:Property |any;
  action: string="";
  flag:boolean=false;

  public show:boolean = false;
  public hide:boolean = true;
  public buttonName:any = 'Show';
  public nameT:any = 'Hide';
  @Input()
  reservation:Reservation ={
    reserveId:0,
    tenantEmail:"",
    reservedStatus:"",
    checkInDate:"",
    checkOutDate:"",
    name:"",
    propertyName:""
  };
  @Input()
  property:Cart ={
    propertyId: 0,
    propertyName: "",
    propertyRating: 0,
    price:0,
    features:"",
    cityName: '',
    address: '',
    image: '',
    tenantEmail:''
  };
  @Output()
  reservationAddedEvent = new EventEmitter();
 @Output()
  propertyAddedEvent = new EventEmitter();
  
  constructor(private httpClientService: HttpClientService,
    private router: Router,private service:SearchServiceService) { }

  ngOnInit(): void {
    
    this.loadproperties();
    
   
  }
  addReservation() {
    this.show=this.show;
    if(this.show)
    {
    this.httpClientService.addReservation(this.reservation).subscribe(
      (reservation) => {
        this.reservationAddedEvent.emit();
        this.msg="Booked Successfully";
        this.router.navigate(['propertytype']);
        
      }
    );
  }
  else{
    this.buttonName = "Show"; 
  }
}
  ser()
  {
    this.service.ser(this.search.propertyName).subscribe(data=>{
    this.Featched=true,  
    this.search=data})
  }
  click : boolean = false;

  onButtonClick(){
    this.click = !this.click;
  }
  userreservationform()
  {
    this.click=!this.click;
  }
  toggle()
  {
    this.show=!this.show;
    if(this.show)
    {
      
      this.buttonName= "Hide";

    }
    else{
      this.buttonName = "Show";
    }
  }
  Hide()
  {
    this.hide=!this.hide;
    if(this.hide)
    {
      
      this.nameT= "Hide";

    }
    else{
      this.nameT = "Show";
    }
  }




  serv()
{
this.flag=true;
}
handleSuccessfulResponse(response:any) {
 this.properties = response;
}
  loadproperties(): void{
    // console.log("event fired")
    this.httpClientService.getProperties().subscribe(res=>this.properties=res);
    
   }
   addProperty(property:Property) {
     console.log(this.property)
    this.httpClientService.addCart(this.property).subscribe(
      (property) => {
        this.propertyAddedEvent.emit();
      
      }
    );
  }
}
