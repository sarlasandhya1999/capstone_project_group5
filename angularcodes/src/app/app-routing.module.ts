import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { OwnersComponent } from './admin/owners/owners.component';
import { PropertyComponent } from './admin/property/property.component';
import { CoroselComponent } from './corosel/corosel.component';
import { FeturesComponent } from './fetures/fetures.component';

import { PropertyComponent1 } from './property1/property1.component';
import { UloginComponent } from './ulogin/ulogin.component';
import { FeatureComponent } from './admin/feature/feature.component';
import { MessageComponent } from './admin/message/message.component';
import { MenuComponent } from './menu/menu.component';
import { ReservationComponent } from './admin/reservation/reservation.component';
import { AddownerComponent } from './admin/owners/addowner/addowner.component';
import { SearchComponent } from './search/search.component';
import { AddreservationComponent } from './admin/reservation/addreservation/addreservation.component';
import { TenantLoginComponent } from './tenant-login/tenant-login.component';
import { UserpropertyComponent } from './userproperty/userproperty.component';
import { UserregisterComponent } from './userregister/userregister.component';
import { UserreservationComponent } from './userreservation/userreservation.component';
import { UserviewComponent } from './userview/userview.component';
const routes: Routes = [
  {path:"features", component:FeturesComponent},{path:"propertytype", component:PropertyComponent1},

  {path:"ulogin", component:UloginComponent},{ path: 'admin/owners', component: OwnersComponent },
  { path: 'ulogin/admin/owners/addowner', component: AddownerComponent },
  { path: 'admin/properties', component: PropertyComponent },{ path: 'admin/properties/viewproperty', component: PropertyComponent },
{path:'',component:CoroselComponent},
  { path: 'admin/features', component: FeatureComponent },
  { path: 'search', component: SearchComponent },
  { path: 'admin/reservations', component: ReservationComponent },
  { path: 'admin/reservations/addreservations', component: AddreservationComponent },
  
 {path:'messageview',component:UserviewComponent},
  { path: 'search/admin/reservations/addreservation', component: AddreservationComponent },
 { path: 'admin/messages', component:MessageComponent},{ path: 'ownerlogin', component:MenuComponent},
 {path:"search/tlogin/userregister", component:UserregisterComponent},
 {path:"search/tlogin", component:TenantLoginComponent},
 {path:"userproperty", component:UserpropertyComponent},{path:"userreservation",component:UserreservationComponent}
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
