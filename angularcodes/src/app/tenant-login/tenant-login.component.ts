import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClientService } from '../service/http-client.service';
import { Tenant } from '../tenant';

@Component({
  selector: 'app-tenant-login',
  templateUrl: './tenant-login.component.html',
  styleUrls: ['./tenant-login.component.css']
})
export class TenantLoginComponent implements OnInit {
  tenants=new Tenant();
  msg='';
    constructor(private httpClientService: HttpClientService,private router:Router) { }
  
    ngOnInit(): void {
    }
  loginUser()
  {
  this.httpClientService.loginTenantFromRemote(this.tenants).subscribe(
    data =>{ console.log("response recived");
  this.router.navigate(['userreservation'])   },
    error => {console.log("exception occured");
  this.msg="Bad credentials,please add proper credentials";
  }
  )
  }

}
