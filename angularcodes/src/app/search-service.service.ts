import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { Observable } from 'rxjs';

import { Property } from './Owner';
@Injectable({
  providedIn: 'root'
})
export class SearchServiceService {

  constructor(private http:HttpClient) { }
  ser(sname:string):Observable<Property>
  {
    return this.http.get<Property>(`http://localhost:8080/owners/getProperty/${sname}`);
  }
}
