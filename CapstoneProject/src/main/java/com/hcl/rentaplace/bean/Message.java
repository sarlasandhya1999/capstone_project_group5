package com.hcl.rentaplace.bean;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;
import lombok.NoArgsConstructor;

@Entity(name="message_table")
@Data
@NoArgsConstructor
public class Message {
	@Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
	private int messageId;
	private String message;
	private String reply;
	
	public int getMessageId() {
		return messageId;
	}
	
	public void setMessageId(int messageId) {
		this.messageId = messageId;
	}
	
	public String getMessage() {
		return message;
	}
	
	public void setMessage(String message) {
		this.message = message;
	}
	
	public String getReply() {
		return reply;
	}
	
	public void setReply(String reply) {
		this.reply = reply;
	}
	
	public List<User> getTenantMessage() {
		return userMessage;
	}


	public void setTenantMessage(List<User> tenantMessage) {
		this.userMessage = tenantMessage;
	}


	public List<Owner> getOwner() {
		return owner;
	}


	public void setOwner(List<Owner> owner) {
		this.owner = owner;
	}


	@JsonIgnore
	@ManyToMany(mappedBy = "messageList",cascade = CascadeType.ALL)
    private List<User> userMessage;

	
	@JsonIgnore
	@ManyToMany(mappedBy = "ownerReply",cascade = CascadeType.ALL)
    private List<Owner> owner;
	
}
