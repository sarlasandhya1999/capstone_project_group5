package com.hcl.rentaplace.bean;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.hcl.rentaplace.bean.Feature;
import com.hcl.rentaplace.bean.Owner;
import com.hcl.rentaplace.bean.Reservation;
import com.hcl.rentaplace.bean.User;

import lombok.Data;
import lombok.NoArgsConstructor;

@Entity(name="property_table")
@Data
@NoArgsConstructor
public class Property {
	@Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
	private int propertyId;
	private String propertyName;
	private double propertyRating;
	private double price;
	private String cityName;
	private String address;
	private String features;
	private String image;
	
	
	
	public int getPropertyId() {
		return propertyId;
	}

	public void setPropertyId(int propertyId) {
		this.propertyId = propertyId;
	}

	public String getPropertyName() {
		return propertyName;
	}

	public void setPropertyName(String propertyName) {
		this.propertyName = propertyName;
	}

	public double getPropertyRating() {
		return propertyRating;
	}

	public void setPropertyRating(double propertyRating) {
		this.propertyRating = propertyRating;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public String getCityName() {
		return cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getFeatures() {
		return features;
	}

	public void setFeatures(String features) {
		this.features = features;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public List<Feature> getFeatureList() {
		return featureList;
	}

	public void setFeatureList(List<Feature> featureList) {
		this.featureList = featureList;
	}

	public List<Reservation> getReserve() {
		return reserve;
	}

	public void setReserve(List<Reservation> reserve) {
		this.reserve = reserve;
	}

	public List<Owner> getOwner() {
		return owner;
	}

	public void setOwner(List<Owner> owner) {
		this.owner = owner;
	}

	public User getUserProperty() {
		return userProperty;
	}

	public void setUserProperty(User tenantProperty) {
		this.userProperty = userProperty;
	}

	@ManyToMany
	@JoinTable(
			name="property_features",
		    joinColumns = {@JoinColumn(name="property_id")},
		    inverseJoinColumns = {@JoinColumn(name="features_id")}
			)
    private List<Feature> featureList;
	
	@JsonIgnore
	@ManyToMany(mappedBy = "reservedPropertyList",cascade = CascadeType.ALL)
	private List<Reservation> reserve;

	@JsonIgnore
	@ManyToMany(mappedBy = "ownerProperty",cascade = CascadeType.ALL)
    private List<Owner> owner;
	
	 @OneToOne(cascade=CascadeType.MERGE)
	    @JoinColumn(name="user_id")
	    private User userProperty;

}
