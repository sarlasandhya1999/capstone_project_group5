package com.hcl.rentaplace.bean;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;


import lombok.Data;
import lombok.NoArgsConstructor;

@Entity(name="owner_table")
@Data
@NoArgsConstructor
public class Owner {
	@Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
	private int ownerId;
	private String ownerName;
	private String ownerPassword;
	private String ownerEmail;
	private double ownerContact;
	

	public int getOwnerId() {
		return ownerId;
	}

	public void setOwnerId(int ownerId) {
		this.ownerId = ownerId;
	}

	public String getOwnerName() {
		return ownerName;
	}

	public void setOwnerName(String ownerName) {
		this.ownerName = ownerName;
	}

	public String getOwnerPassword() {
		return ownerPassword;
	}

	public void setOwnerPassword(String ownerPassword) {
		this.ownerPassword = ownerPassword;
	}

	public String getOwnerEmail() {
		return ownerEmail;
	}

	public void setOwnerEmail(String ownerEmail) {
		this.ownerEmail = ownerEmail;
	}

	public double getOwnerContact() {
		return ownerContact;
	}

	public void setOwnerContact(double ownerContact) {
		this.ownerContact = ownerContact;
	}

	public List<Message> getOwnerRply() {
		return ownerReply;
	}

	public void setOwnerRply(List<Message> ownerRply) {
		this.ownerReply = ownerRply;
	}

	public List<Property> getOwnerProperty() {
		return ownerProperty;
	}

	public void setOwnerProperty(List<Property> ownerProperty) {
		this.ownerProperty = ownerProperty;
	}


	@ManyToMany
	@JoinTable(
			name="owner_message",
		    joinColumns = {@JoinColumn(name="owner_id")},
		    inverseJoinColumns = {@JoinColumn(name="message_id")}
			)
    private List<Message> ownerReply;
	
	@ManyToMany
	@JoinTable(
			name="owner_property",
		    joinColumns = {@JoinColumn(name="owner_id")},
		    inverseJoinColumns = {@JoinColumn(name="property_id")}
			)
    private List<Property> ownerProperty;

}
