package com.hcl.rentaplace.bean;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;

import com.hcl.rentaplace.bean.Message;

import lombok.Data;
import lombok.NoArgsConstructor;

@Entity(name="user_table")
@Data
@NoArgsConstructor
public class User {
	@Id
	@GeneratedValue(strategy= GenerationType.IDENTITY)
	private int userId;
	private String userName;
	private String userPassword;
	private String userEmail;
	private double userContactNo;
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getUserPassword() {
		return userPassword;
	}
	public void setUserPassword(String userPassword) {
		this.userPassword = userPassword;
	}
	public String getUserEmail() {
		return userEmail;
	}
	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}
	public double getUserContactNo() {
		return userContactNo;
	}
	public void setUserContactNo(double userContactNo) {
		this.userContactNo = userContactNo;
	}
	
	public List<Message> getMessageList() {
		return messageList;
	}


	public void setMessageList(List<Message> messageList) {
		this.messageList = messageList;
	}


	@ManyToMany
	@JoinTable(
			name="user_message",
		    joinColumns = {@JoinColumn(name="user_id")},
		    inverseJoinColumns = {@JoinColumn(name="message_id")}
			)
    private List<Message> messageList;
	
   
	
}
