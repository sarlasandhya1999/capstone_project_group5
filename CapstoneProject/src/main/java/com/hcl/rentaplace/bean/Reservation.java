package com.hcl.rentaplace.bean;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;

import com.hcl.rentaplace.bean.Property;

import lombok.Data;
import lombok.NoArgsConstructor;

@Entity(name="reservation_table")
@Data
@NoArgsConstructor
public class Reservation {
	 @Id
	    @GeneratedValue(strategy= GenerationType.IDENTITY)
		private int reserveId;
		private String reservedStatus;
		private String checkInDate;
		private String checkOutDate;
		


		public int getReserveId() {
			return reserveId;
		}



		public void setReserveId(int reserveId) {
			this.reserveId = reserveId;
		}



		public String getReservedStatus() {
			return reservedStatus;
		}



		public void setReservedStatus(String reservedStatus) {
			this.reservedStatus = reservedStatus;
		}



		public String getCheckInDate() {
			return checkInDate;
		}



		public void setCheckInDate(String checkInDate) {
			this.checkInDate = checkInDate;
		}



		public String getCheckOutDate() {
			return checkOutDate;
		}



		public void setCheckOutDate(String checkOutDate) {
			this.checkOutDate = checkOutDate;
		}



		public List<Property> getReservedPropertyList() {
			return reservedPropertyList;
		}



		public void setReservedPropertyList(List<Property> reservedPropertyList) {
			this.reservedPropertyList = reservedPropertyList;
		}



		@ManyToMany
		@JoinTable(
				name="property_reserve",
			    joinColumns = {@JoinColumn(name="reservation_id")},
			    inverseJoinColumns = {@JoinColumn(name="property_id")}
				)
	    private List<Property> reservedPropertyList;
		    
}
