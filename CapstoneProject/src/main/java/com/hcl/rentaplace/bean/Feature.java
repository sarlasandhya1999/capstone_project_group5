package com.hcl.rentaplace.bean;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;
//import com.hcl.rentaplace.bean.Property;

import lombok.Data;
import lombok.NoArgsConstructor;

@Entity(name="feature_table")
@Data
@NoArgsConstructor
public class Feature {
	    @Id
	    @GeneratedValue(strategy= GenerationType.IDENTITY)
		private int featureId;
		private String featureName;
		
		public int getFeatureId() {
			return featureId;
		}
		
		public void setFeatureId(int featureId) {
			this.featureId = featureId;
		}
		
		public String getFeatureName() {
			return featureName;
		}
		
		public void setFeatureName(String featureName) {
			this.featureName = featureName;
		}
		public List<Property> getProperty() {
			return property;
		}


		public void setProperty(List<Property> property) {
			this.property = property;
		}


		@JsonIgnore
		@ManyToMany(mappedBy = "featureList",cascade = CascadeType.ALL)
	    private List<Property> property;
		
		
}
