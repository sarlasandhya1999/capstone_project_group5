package com.hcl.rentaplace.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.rentaplace.exception.IdNotFoundException;
import com.hcl.rentaplace.exception.CapstoneException;
import com.hcl.rentaplace.bean.Owner;
import com.hcl.rentaplace.service.OwnerService;
import com.hcl.rentaplace.service.UserService;

@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping(path = "owners")
@RestController
public class OwnerController {
	@Autowired
	OwnerService ownerService;

	@Autowired
	UserService userService;
	
	 @PostMapping("login")
	 public Owner loginOwner(@RequestBody Owner owner) throws Exception {
	 String tempEmailId =owner.getOwnerName();
	 String tempPwd =owner.getOwnerPassword();
	 Owner userObj = null;
	 if(tempEmailId !=null && tempPwd !=null) {
	 userObj=ownerService.findByName(tempEmailId,tempPwd);
	 }
	 if(userObj==null) {
	 throw new Exception("Invalid Login Credentials");



	 }
	 return userObj;
	 }
	 @RequestMapping(method = RequestMethod.GET, value = "getOwners")
		public ResponseEntity<List<Owner>> getOwners() {
			return new ResponseEntity<List<Owner>>(ownerService.displayOwners(), HttpStatus.OK);
		}

	 @RequestMapping(method = RequestMethod.GET, value = "getOwner/{id}")
		public ResponseEntity<Owner> getOwnerById(@PathVariable("id") Integer id) throws IdNotFoundException {
			return new ResponseEntity<Owner>(ownerService.getOwnerById(id), HttpStatus.OK);
		}

		@RequestMapping(method = RequestMethod.POST, value = "addOwner")
		public ResponseEntity<Owner> addOwner(@RequestBody Owner owner) throws CapstoneException{
			return new ResponseEntity<Owner>(ownerService.addOwner(owner), HttpStatus.CREATED);
		}

		@RequestMapping(method = RequestMethod.DELETE, value = "deleteOwner/{id}")
		public ResponseEntity<String> deleteOwner(@PathVariable("id") int id) throws IdNotFoundException{
			return new ResponseEntity<String>(ownerService.deleteOwner(id), HttpStatus.OK);
		}

		@RequestMapping(method = RequestMethod.PUT, value = "updateOwner/{id}")
		public ResponseEntity<Owner> updateOwner(@RequestBody Owner owner, @PathVariable("id") int id) 		throws IdNotFoundException  {
			return new ResponseEntity<Owner>(ownerService.updateOwner(owner, id), HttpStatus.OK);
		}

	

}
