package com.hcl.rentaplace.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.rentaplace.bean.User;
import com.hcl.rentaplace.exception.CapstoneException;
import com.hcl.rentaplace.exception.IdNotFoundException;
import com.hcl.rentaplace.service.UserService;

@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping(path = "users")
@RestController
public class UserController {
	
	@Autowired
	UserService userService;
	
	 @PostMapping("login")
	 public User loginOwner(@RequestBody User user) throws Exception {
	 String tempEmailId =user.getUserName();
	 String tempPwd =user.getUserPassword();
	 User userObj = null;
	 if(tempEmailId !=null && tempPwd !=null) {
	 userObj=userService.findByName(tempEmailId,tempPwd);
	 }
	 if(userObj==null) {
	 throw new Exception("Invalid Login Credentials");



	 }
	 return userObj;
	 }
	 @RequestMapping(method = RequestMethod.POST, value = "registerUser")
		public ResponseEntity<User> registerUser(@RequestBody User user) throws CapstoneException {
			return new ResponseEntity<User>(userService.addUser(user),HttpStatus.CREATED);
		}
		
		@RequestMapping(method = RequestMethod.DELETE, value = "deleteUser/{id}")
		public ResponseEntity<String> deleteUser(@PathVariable("id") int id) throws IdNotFoundException {
			return new ResponseEntity<String>(userService.deleteUser(id), HttpStatus.OK);
		}
		
		@RequestMapping(method = RequestMethod.PUT, value = "updateUser/{id}")
		public ResponseEntity<User> updateUser(@RequestBody User user,@PathVariable("id") int id) throws IdNotFoundException {
			return new ResponseEntity<User>(userService.updateUser(user, id), HttpStatus.OK);
		}
	 

}
