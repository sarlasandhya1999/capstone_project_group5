package com.hcl.rentaplace.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


import com.hcl.rentaplace.bean.User;
@Repository
public interface UserRepository  extends JpaRepository<User,Integer> {
	public User findByUserNameAndUserPassword(String username, String password);
	

}
