package com.hcl.rentaplace.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.hcl.rentaplace.bean.Owner;

@Repository
public interface OwnerRepository extends JpaRepository<Owner,Integer> {
	
	public Owner findByOwnerNameAndOwnerPassword(String username, String password);


}
