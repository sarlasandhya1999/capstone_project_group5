package com.hcl.rentaplace.service;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hcl.rentaplace.bean.Owner;
import com.hcl.rentaplace.bean.User;
import com.hcl.rentaplace.dao.OwnerRepository;
import com.hcl.rentaplace.dao.UserRepository;
import com.hcl.rentaplace.exception.CapstoneException;
import com.hcl.rentaplace.exception.IdNotFoundException;
@Service
public class OwnerServiceImpl  implements OwnerService {
	@Autowired
	OwnerRepository ownerRepository ;
	
	@Autowired
	UserRepository TenantRepository ;
	
	@Override
	public Owner addOwner(Owner owner) throws CapstoneException {
		// TODO Auto-generated method stub
		return ownerRepository.save(owner);
	}

	@Override
	public List<Owner> displayOwners() {
		// TODO Auto-generated method stub
		return ownerRepository.findAll();
	}

	@Override
	public Owner getOwnerById(Integer id) throws IdNotFoundException {
		// TODO Auto-generated method stub
		Owner owner=ownerRepository.findById(id).orElseThrow(()->new IdNotFoundException("Owner Id not found"));
		return owner;
	}

	@Override
	public String deleteOwner(Integer id) throws IdNotFoundException {
		// TODO Auto-generated method stub
		Owner owner=ownerRepository.findById(id).orElseThrow(()->new IdNotFoundException("Owner Id not found"));
		ownerRepository.delete(owner);
		return "Owner Id is deleted successfully";
	}

	@Override
	public Owner updateOwner(Owner owner, int id) throws IdNotFoundException {
		// TODO Auto-generated method stub
		ownerRepository.findById(id).orElseThrow(()->new IdNotFoundException("Owner Id not found"));
		return ownerRepository.saveAndFlush(owner);
	}

	@Override
	public Owner findByName(String username, String password) {
		// TODO Auto-generated method stub
		return null;
	}
	
	}
	
