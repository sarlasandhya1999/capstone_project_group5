package com.hcl.rentaplace.service;

import java.util.ArrayList;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hcl.rentaplace.bean.User;
import com.hcl.rentaplace.dao.UserRepository;
import com.hcl.rentaplace.exception.IdNotFoundException;
import com.hcl.rentaplace.exception.CapstoneException;

@Service
public class UserServiceImpl implements UserService {
	
	@Autowired
	UserRepository userRepository ;
	
	@Override
	public User addUser(User user){
		// TODO Auto-generated method stub
		List<User> UserList = new ArrayList<>();
		UserList.add(user);
		return userRepository.save(user);
	}

	
	@Override
	public String deleteUser(Integer id) throws IdNotFoundException {
		// TODO Auto-generated method stub
		User user=userRepository.findById(id).orElseThrow(()->new IdNotFoundException("User Id not found"));
		userRepository.delete(user);
		return "User Id is deleted successfully";
	}
	

	@Override
	public User updateUser(User user, int id) throws IdNotFoundException {
		
	   userRepository.findById(id).orElseThrow(()-> new IdNotFoundException("user Id not found"));
		return userRepository.saveAndFlush(user);
	}
	



	@Override
	public User findByName(String username, String password){

	return userRepository.findByUserNameAndUserPassword(username,password);

	}


}
