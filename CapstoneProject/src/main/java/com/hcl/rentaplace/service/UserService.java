package com.hcl.rentaplace.service;

import org.springframework.stereotype.Service;

import com.hcl.rentaplace.exception.IdNotFoundException;
import com.hcl.rentaplace.exception.CapstoneException;

import com.hcl.rentaplace.bean.User;

@Service
public interface UserService {
	public User addUser(User user);
	public String deleteUser(Integer id) throws  IdNotFoundException;
	public User updateUser(User user, int id) throws  IdNotFoundException;
	public User findByName(String username, String password);
}

