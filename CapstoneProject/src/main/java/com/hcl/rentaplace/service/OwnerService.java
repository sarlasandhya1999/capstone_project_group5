package com.hcl.rentaplace.service;

import java.util.List;

import org.springframework.stereotype.Service;
import com.hcl.rentaplace.bean.Owner;
import com.hcl.rentaplace.bean.User;
import com.hcl.rentaplace.exception.CapstoneException;
import com.hcl.rentaplace.exception.IdNotFoundException;
@Service
public interface OwnerService {
	public Owner findByName(String username, String password);
	public Owner addOwner(Owner owner) throws CapstoneException;
	public List<Owner> displayOwners();
	public Owner getOwnerById(Integer id) throws IdNotFoundException;
	public String deleteOwner(Integer id) throws IdNotFoundException;
	public Owner updateOwner(Owner owner, int id) throws IdNotFoundException;
	
//	public List<User> displayUser();
//	public User getUserById(Integer id);
//	public String deleteUser(Integer id);

}
