package com.hcl.rentaplace.exception;

public class CapstoneException extends Exception{
	
	
	private String msg;
	
	public CapstoneException(String msg) {
		this.msg= msg;
	}
	@Override
	 public String getMessage() {
	        return this.msg;
	    }

}
